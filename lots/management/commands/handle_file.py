from django.core.management.base import BaseCommand, CommandError
from sys import stdin, stdout
from argparse import FileType
from lots.utils import *

class Command(BaseCommand):
	help = 'parse input file for commands'

	def add_arguments(self,parser):
		parser.add_argument('input', nargs='?', type=FileType('r'),
                            default=stdin)

	def handle(self,*args,**options):
		input = options['input']
		for r in input:
			handle_command(r)


