from .models import Lots
from django.core.exceptions import ObjectDoesNotExist

def handle_command(line):
	parts = line.split(' ')
	if parts[0]=="create_parking_lot":
		try:
			number = int(parts[1])
			for i in range(0,number):
				lots = Lots.objects.create(status=False)
			print "Created a parking lot with %d slots" %(number) 
		except Exception as e:
			print "Can't create non integer parking lots"
	elif parts[0]=="park":
		registration_number = parts[1]
		color = parts[2]
		try:
			check_already = Lots.objects.get(registration_id=registration_number)
			print "Car already in the slot"
		except ObjectDoesNotExist as e:
			try:
				available_lot = Lots.objects.all().filter(status=False).order_by('lot_id')[0]
				available_lot.registration_id = registration_number
				available_lot.color = color
				available_lot.status = True
				available_lot.save()
				print "Allocated slot number: %s" %(available_lot.lot_id)
			except ObjectDoesNotExist as e:
				print "All parking lots are busy"
	elif parts[0]=="leave":
		try:
			lot_number = parts[1]
			occupied_lot = Lots.objects.get(lot_id=lot_number,status=True)
			occupied_lot.registration_id= None
			occupied_lot.color = None
			occupied_lot.status = False
			occupied_lot.save()
			print "Slot Number %s is free" %(occupied_lot.lot_id)
		except Exception as e:
			print "Lot you are trying to be empty is already empty or invalid lot number"
	elif parts[0]=="status":
		occupied_lots = Lots.objects.all().filter(status=True)
		if len(occupied_lots)==0:
			print "All lots are empty"
		else:
			print "Slot no registration_number color"
			for lot in occupied_lots:
				print str(lot.lot_id)+" "+lot.registration_id+" "+lot.color
	elif parts[0]=="registration_numbers_for_cars_with_color":
		color = parts[1]
		try:
			occupied_lots = Lots.objects.all().filter(color=color)
			for lot in occupied_lots:
				print lot.registration_id
		except ObjectDoesNotExist as e:
			print "No car of given color"
	elif parts[0]=="slots_numbers_for_cars_with_color":
		color = parts[1]
		try:
			occupied_lots = Lots.objects.all().filter(color=color)
			for lot in occupied_lots:
				print lot.lot_id
		except ObjectDoesNotExist as e:
			print "No car of given color"
	elif parts[0]=="slots_numbers_for_registration_number":
		registration_number = parts[1]
		try:
			occupied_lots = Lots.objects.get(registration_id=registration_number)
			print occupied_lots.lot_id
		except Exception as e:
			print "No car with registration number %s" %(registration_number)
	else:
		print "Command not recognised"


