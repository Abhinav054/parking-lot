# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Lots(models.Model):
	lot_id = models.AutoField(primary_key=True)
	registration_id = models.CharField(max_length=64,blank=True,null=True)
	color = models.CharField(max_length=64,blank=True,null=True)
	status = models.BooleanField(default=False)